import fetch from 'node-fetch'
import 'dotenv/config'

// Weekly Data from Alpha Vantage
async function getData(stock) {
  // const dataUrl = `https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=${stock}&interval=30min&outputsize=compact&apikey=${process.env.apiKey2}`;
  // const dailyDataURL = `https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=${stock}&apikey=${process.env.apiKey2}`;
  const weeklyDataURL = `https://www.alphavantage.co/query?function=TIME_SERIES_WEEKLY&symbol=${stock}&apikey=${process.env.apiKey2}`
  return fetch(weeklyDataURL)
    .then(res => res.json())
    .then(data => data)
}

export default function bestPerformersData(arr) {
  // let performersData = [];
  console.log('\n\n\n ||||| Scraped these top performers', arr)
  const returnArr = []
  arr.forEach(i => {
    getData(i.symbol).then(item =>
      returnArr.push({
        stock: i,
        item
      })
    )

    if (arr === returnArr) {
      return returnArr
    }
  })
}
