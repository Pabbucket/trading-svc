import puppeteer from 'puppeteer';

export default async function scrapeStocks(url) {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await page.goto(url);
  let rawData = [];

  const topPetformers = await page.evaluate(() => {
    const rows =
      '#main-content-column div.barchart-content-block div.bc-table-wrapper table > tbody > tr';
    return [...document.querySelectorAll(rows)].length;
  });

  const rowsXref =
    '//*[@id="main-content-column"]/div/div[5]/div/div[2]/div/div/ng-transclude/table/tbody/tr';

  for (let i = 0; i < topPetformers; i++) {
    const [elSym] = await page.$x(`${rowsXref}[${i + 1}]/td[1]/div/span[2]/a`);
    const [elName] = await page.$x(`${rowsXref}[${i + 1}]/td[2]/div/span/span/span`);
    const [elPrice] = await page.$x(`${rowsXref}[${i + 1}]/td[3]/div/span/span/span`);
    const [elMax] = await page.$x(`${rowsXref}[${i + 1}]/td[5]/div/span/span/span`);

    const symTitle = await elSym.getProperty('textContent');
    const symTitleText = await symTitle.jsonValue();

    const nameTitle = await elName.getProperty('textContent');
    const nameTitleText = await nameTitle.jsonValue();

    const namePrice = await elPrice.getProperty('textContent');
    const namePriceText = await namePrice.jsonValue();

    const nameMax = await elMax.getProperty('textContent');
    const nameMaxText = await nameMax.jsonValue();
    //get link value for further sctaping
    const symHref = await elSym.getProperty('href');
    const symHrefText = await symHref.jsonValue();
    const symHrefClean = `${symHrefText.replace('/overview', '/opinion')}`;

    const finalObj = {
      symbol: symTitleText,
      href: symHrefClean,
      name: nameTitleText,
      price: namePriceText,
      perChange: nameMaxText
    };
    rawData = [...rawData, finalObj];
  }
  const cleanData = rawData.filter(
    i => parseInt(i.price) > 100 && parseFloat(i.perChange.replace('+', '').replace('%', '')) > 0.5
  );
  browser.close();
  return cleanData.slice(0, 5);
}
