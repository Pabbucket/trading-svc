import express from 'express'
// import bestPerformersData from './utils/bestPerformersData'
import tradeTime from './utils/tradeTime'
// import { performersRoute } from '../routes/performersRoute'
import scrapeStocks from './utils/scrapeStocks'
// import bodyParser from 'body-parser';

const app = express()
const PORT = 8000
// app.use(bodyParser.json());
// app.use('/', home)
// app.use('/performers', performersRoute)

// let times = 0;
// setInterval(function() {
// times++;

if (tradeTime()) {
  scrapeStocks(
    'https://www.barchart.com/stocks/sectors/hot-prospects-stocks?viewName=main&orderBy=percentChange&orderDir=desc'
  ).then(
    app.get('/', (req, res) => {
      console.log(res)
      // res.send(arr)
    })
    // bestPerformersData(arr).then(i => {
    //   console.log({ i })
    //   app.get('/', (req, res) => {
    //     res.send(i)
    //   })
    // })
  )
}

// }, 15000);
// TODO: remove this!!!
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

app.listen(PORT, () => {
  console.log(`Server is listening on port: ${PORT}`)
})
